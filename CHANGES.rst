.. _changes:

Changes
=======

0.9.9 - 2022-01-10
------------------
- Fix documentation code samples `#15`_
- Fix handling of padding after empty sequences `#14`_
- Support conversion from rosbag2 to rosbag1 `#11`_

.. _#11: https://gitlab.com/ternaris/rosbags/issues/11
.. _#14: https://gitlab.com/ternaris/rosbags/issues/14
.. _#15: https://gitlab.com/ternaris/rosbags/issues/15


0.9.8 - 2021-11-25
------------------
- Support bool and float constants in msg files


0.9.7 - 2021-11-09
------------------
- Fix parsing of const fields with string value `#9`_
- Parse empty msg definitions
- Make packages PEP561 compliant
- Parse msg bounded fields and default values `#12`_

.. _#9: https://gitlab.com/ternaris/rosbags/issues/9
.. _#12: https://gitlab.com/ternaris/rosbags/issues/12

0.9.6 - 2021-10-04
------------------
- Do not match msg separator as constant value


0.9.5 - 2021-10-04
------------------
- Add string constant support to msg parser


0.9.4 - 2021-09-15
------------------
- Make reader1 API match reader2
- Fix connection mapping for reader2 messages `#1`_, `#8`_

.. _#1: https://gitlab.com/ternaris/rosbags/issues/1
.. _#8: https://gitlab.com/ternaris/rosbags/issues/8

0.9.3 - 2021-08-06
------------------

- Add const fields to type classes
- Add CDR to ROS1 bytestream conversion
- Add ROS1 message definiton generator
- Use connection oriented APIs in readers and writers
- Add rosbag1 writer


0.9.2 - 2021-07-08
------------------

- Support relative type references in msg files


0.9.1 - 2021-07-05
------------------

- Use half-open intervals for time ranges
- Create appropriate QoS profiles for latched topics in converted bags
- Fix return value tuple order of messages() in documentation `#2`_
- Add type hints to message classes
- Remove non-default ROS2 message types
- Support multi-line comments in idl files
- Fix parsing of msg files on non-POSIX platforms `#4`_

.. _#2: https://gitlab.com/ternaris/rosbags/issues/2
.. _#4: https://gitlab.com/ternaris/rosbags/issues/4


0.9.0 - 2021-05-16
------------------

- Initial Release
